<?php

namespace App\Http\Controllers;

use App\Parser;
use App\Query;
use Illuminate\Http\Request;

class ParseController extends Controller
{
    public function parseText(Request $r) {
        $raw = (string)$r->get('raw', '');

        try {
            $q = Query::parseText($raw);

            $parser = new Parser();

            $content = $parser->fetchData($q->getUrl());

            if ($content !== null) {
                $text = $parser->parseAsXML($content, $q);
                return view('welcome', [
                    'result' => $text,
                    'raw' => $raw,
                ]);
            } else {
                return view('welcome', [
                    'empty' => true,
                    'raw' => $raw,
                ]);
            }
        } catch (\Exception $e) {
            return view('welcome', [
                'error' => $e->getMessage(),
                'raw' => $raw,
            ]);
        }
    }
}
