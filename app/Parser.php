<?php


namespace App;


use DOMDocument;
use DOMXPath;

class Parser
{
    const ALLOW_TAGS = ['p', 'table', 'tr', 'th', 'td', 'tbody', 'thead'];

    public function fetchData($url) : string
    {
        // Сделаем вид типа мы браузер
        // и скачаем все-все
        $opts = [
            "http" => [
                "method" => "GET",
                "header" => "Authority: stackoverflow.com\r\n" .
                    "Pragma: no-cache\r\n" .
                    "Cache-control: no-cache\r\n" .
                    "User-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36\r\n" .
                    "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8\r\n" .
                    "Referer: https://www.google.ru/\r\n" .
                    "Accept-language: ru,en;q=0.9,cs;q=0.8,en-US;q=0.7,fa;q=0.6\r\n"
            ]
        ];

        $context = stream_context_create($opts);

        $data = file_get_contents($url, false, $context);

        return $data;
    }

    public function parseAsXML(string $string, Query $q) : string {
        $doc = new DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTML($string);
        libxml_clear_errors();

        $xpath = new DOMXPath($doc);

        $entries = $xpath->query($q->getXPath());

        $text = [];

        for($i = 0; $i < $entries->length; $i++) {
            $node = $entries->item($i);
            $text[] = $this->getNodeAsText($node);
        }

        return implode("\n", $text);
    }

    public function getNodeAsText( \DOMElement $node) : string {
        $tag = $node->tagName;
        if (in_array($tag, self::ALLOW_TAGS)) {
            return "<{$tag}>".trim($node->nodeValue)."</{$tag}>";
        } else {
            return trim($node->nodeValue);
        }
    }
}