<?php


namespace App;


class PathItem
{
    const ID_TYPE = 'id';
    const CLASS_TYPE = 'class';
    protected $type;
    protected $value;

    public function __construct(string $type, string $value)
    {
        $this->type = $type;
        $this->value = $value;
    }

    /**
     * @param $raw
     * @return PathItem
     * @throws \Exception
     */
    public static function fromRaw($raw) : self {
        $data = explode("=", $raw, 2);

        if (count($data) !== 2) {
            throw new \Exception("Invalid raw value, must be id=\"value\" or class=\"value\"");
        }

        $_type = trim(mb_strtolower($data[0]));
        $_value =  mb_substr($data[1], 1, mb_strlen($data[1])-2);

        if ($_type === self::ID_TYPE) {
            return new self(self::ID_TYPE, $_value);
        } else if ($_type === self::CLASS_TYPE) {
            return new self(self::CLASS_TYPE, $_value);
        } else {
            throw new \Exception("Invalid raw value, must be id=\"value\" or class=\"value\"");
        }
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function toCSS()
    {
        if ($this->type === self::CLASS_TYPE) {
            return ".".$this->value;
        } else if ($this->type === self::ID_TYPE) {
            return "#".$this->value;
        } else {
            throw new \Exception("Unknown type for item ".$this->type);
        }
    }


}