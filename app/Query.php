<?php


namespace App;


use Symfony\Component\CssSelector\CssSelectorConverter;

class Query
{

    const MAGIC_REGEXP_FOR_URL = '#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#';
    const MAGIC_REGEXP_FOR_PATH = '/((id|class)=\"(.*?)\")/im';
    protected $url;
    protected $path;

    /**
     * Query constructor.
     * @param $url
     * @param $path
     */
    public function __construct(string $url, array $path)
    {
        $this->url = $url;
        $this->path = $path;
    }

    /**
     * @param $text
     * @return Query
     * @throws \Exception
     */
    public static function parseText($text): self
    {

        //Надеюсь что url будет в строке один
        $url = [];
        preg_match(self::MAGIC_REGEXP_FOR_URL, $text, $url);
        $url = trim($url[0] ?? '');
        if ($url === '') {
            throw new \Exception("Url is empty!");
        }

        // Из тз не понятно могут ли id=??? class=?? быть до url
        // пускай не могут
        $_text = mb_substr($text, mb_strpos($url, $text));

        $path = [];
        preg_match_all(self::MAGIC_REGEXP_FOR_PATH, $text, $path);
        $path = $path[0]??[];

        $path = array_map( function ($raw) { return PathItem::fromRaw($raw); }, $path );

        if (count($path) === 0) {
            throw new \Exception("Empty path");
        }

        return new self($url, $path);
    }

    public function getUrl()
    {
        return $this->url;
    }

    /*
     * Тут мы генерим xpath запрос из того что мы напарсили из текста
     * должно получиться что-то типа
     * //*[id="value"]/*[class="book"]
     */
    public function getXPath(): string
    {
        $converter = new CssSelectorConverter();
        $q = array_map( function (PathItem $item) {
            return $item->toCSS();
        }, $this->path );

        return $converter->toXPath( implode(' ', $q) );
    }


}