<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Parser</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #222222;
            font-family: 'Raleway', sans-serif;
            margin: 0;
            padding: 10px 15px;
        }


        .form textarea {
            width: 100%;
            box-sizing: border-box;
        }

        .form input[type=submit] {
            border: 1px solid #CCC;
            font-size: 20px;
        }
    </style>
</head>
<body>
<div class="form">
    <form method="POST" action="/parser">
        <label>
            Text to parse<br/>
            <textarea rows="10" name="raw">{{isset($raw)?$raw:""}}</textarea>
        </label>
        <input type="submit" value="Send"/>
    </form>
</div>

@if(isset($result))
    <h2>RESULT</h2>
    {!! $result !!}
@endif
@if(isset($empty))
    <h2>Not fount</h2>
@endif
@if(isset($error))
    <h2 style="color:red">{{$error}}</h2>
@endif
</body>
</html>
