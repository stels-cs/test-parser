<?php

namespace Tests\Feature;

use App\Parser;
use App\Query;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    public function testBasicTest()
    {
        $text = 'asdfasdfvasdf https://www.sunrise-tour.ru/russia/chernoe-more/tury/ id="main_content" asdfasdasf class="main_block_of_content"  asdfasdf  class="mboc_text"  adsfasdfasd';
        
        $testUrl = 'https://www.sunrise-tour.ru/russia/chernoe-more/tury/';
        
        $q = Query::parseText($text);

        $this->assertEquals($q->getUrl(), $testUrl);

        $parser = new Parser();

        $content = $parser->fetchData($q->getUrl());

        if ($content !== null) {
            $text = $parser->parseAsXML($content, $q);
            $this->assertNotEmpty($text);
        }
    }
}
